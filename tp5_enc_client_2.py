import os
import socket
import struct
import sys
import re
import logging

# LOG_FORMAT = "%(asctime)s %(levelname)s %(message)s"
#
# # On check si le dossier existe et sinon on le créé
# if not os.path.exists('/var/log/bs_client'):
#     os.mkdir('/var/log/bs_client')
#
# logging.basicConfig(filename="/var/log/bs_client/bs_client.log", format=LOG_FORMAT,
#                     level=logging.DEBUG)
#
# handler = logging.StreamHandler(sys.stdout)
# handler.setLevel(level=logging.ERROR)
# handler.setFormatter(logging.Formatter("%(levelname)s %(message)s"))
# logging.getLogger().addHandler(handler)

# On définit la destination de la connexion
host = '127.0.0.1'  # IP du serveur
port = 13337  # Port choisir par le serveur

# Création de l'objet socket de type TCP (SOCK_STREAM)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connexion au serveur
try:
    s.connect((host, port))
except Exception as e:
    logging.error(f"Impossible de se connecter au serveur {host} sur le port {port}.")
    exit(1)
# note : la double parenthèse n'est pas une erreur : on envoie un tuple à la fonction connect()

logging.info(f"Connexion réussie à {host}:{port}.")

accepted_regex = r'\d+[\+\-\*]\d+'
serv_input = input('Tapez un calcul :')

if type(serv_input) != str:
    raise TypeError()
if not re.match(accepted_regex, serv_input):
    raise Exception('invalid input: should be an arithmetic expression')

serv_input = serv_input.replace(' ', '')
detected_char = ''
tokens = []

for character in ['+', '-', '*']:
    if character not in serv_input:
        continue

    tokens = serv_input.split(character)
    detected_char = character
    break

first_num = int(tokens[0])
second_num = int(tokens[1])

total_bytes = bytes()
total_bytes += struct.pack('iib', first_num, second_num, bytes(character[0], 'ascii')[0])
total_bytes += "c'est ciao".encode('ascii')

total_bytes = len(total_bytes).to_bytes(1) + total_bytes

s.sendall(total_bytes)
logging.info(f"Message envoyé au serveur {host} : {total_bytes}.")

# On reçoit le résultat sous 4 octets
data = s.recv(4)

# On libère le socket TCP
s.close()

# Affichage de la réponse reçue du serveur
logging.info(f"Réponse reçue du serveur {host} : {int.from_bytes(data)}.")
print(f'Réponse reçue du serveur {host} : {int.from_bytes(data)}')

# On quitte proprement
sys.exit(0)
