import os
import socket
import sys
import argparse
import logging
import re

host = ''
port = 80

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))

print(f'started server on {host}:{port}')
s.listen(1)

while True:
    conn, addr = s.accept()

    try:
        file = conn.makefile('rw')

        req = file.readline()
        reqTokens = req.split(' ')

        if reqTokens[0] == 'GET':
            filename = reqTokens[1]

            # Sécu sécu sécu
            if ".." in filename:
                file.write('HTTP/1.0 404 Not Found\n\n')
                continue

            content = open(f'html/{filename.strip('/')}')
            html_content = content.read()
            content.close()

            file.write(f'HTTP/1.0 200 OK\n\n{html_content}\n')

        file.close()
        conn.close()

    except socket.error as e:
        print(f"Error : {e}")
        break

conn.close()
sys.exit(0)
