import os
import socket
import sys
import argparse
import logging
import re

host = ''
port = 80

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))

print(f'started server on {host}:{port}')
s.listen(1)

while True:
    conn, addr = s.accept()
    print(f'client connected: {addr}')

    try:
        file = conn.makefile('rw')

        req = file.readline()
        reqTokens = req.split(' ')

        if reqTokens[0] == 'GET':
            if reqTokens[1] == '/':
                file.writelines(['HTTP/1.0 200 OK\n\n<h1>Hello je suis un serveur HTTP</h1>\n\n'])

        file.close()
        conn.close()

    except socket.error as e:
        print(f"Error : {e}")
        break

conn.close()
sys.exit(0)
