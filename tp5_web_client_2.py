import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 80))
file = s.makefile('rw')
file.write('GET /index.html HTTP/1.1\n\n')
file.flush()

# On reçoit la string Hello
data = file.read()

print(f'Réponse du serveur HTTP: {data}')
s.close()
