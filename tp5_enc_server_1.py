import os
import socket
import sys
import argparse
import logging
import re

# LOG_FORMAT = "%(asctime)s %(levelname)s %(message)s"
#
# # On check si le dossier existe et sinon on le créé
# if not os.path.exists('/var/log/bs_server'):
#     os.mkdir('/var/log/bs_server')
#
# logging.basicConfig(filename="/var/log/bs_server/bs_server.log", format=LOG_FORMAT,
#                     level=logging.DEBUG)
#
# # Pour afficher les logs dans la console (genre c'est pas actif par défaut ?!)
# handler = logging.StreamHandler(sys.stdout)
# handler.setFormatter(logging.Formatter(LOG_FORMAT))
# logging.getLogger().addHandler(handler)

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', action='store', type=int)
args = parser.parse_args()

if args.port:
    if int(args.port) not in range(0, 65535):
        logging.error(f"Le port spécifié n'est pas un port possible (de 0 à 65535)")
        exit(1)

    if int(args.port) < 1024:
        logging.error(f"Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
        exit(1)

host = ''
port = 13337 if not args.port else int(args.port)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))

logging.info(f"Le serveur tourne sur {host}:{port}")

s.listen(1)
conn, addr = s.accept()
logging.info(f"Un client {addr} s'est connecté.")

while True:

    try:
        length = int.from_bytes(conn.recv(1))
        if length > 100 or length < 13:
            break

        data_str = conn.recv(length).decode()
        if not data_str.endswith("c'est ciao"):
            print("Le client n'a pas envoyé 'c'est ciao', la séquence de fin")
            break

        data_str = data_str.replace("c'est ciao", "")

        isInvalid = ("(" in data_str or ")" in data_str or "[" in data_str or "]" in data_str or "return" in data_str or
                     "break" in data_str or "." in data_str)

        if isInvalid:
            logging.info(f"Le client {addr} a déclenché la protection contre le RCE, déconnexion...")
            conn.sendall(bytes('Protection RCE, vous allez être déconnecté', 'utf-8'))

            break

        result = eval(data_str)

        conn.sendall(result.to_bytes(4))

    except socket.error as e:
        print(f"Error : {e}")
        break

conn.close()
sys.exit(0)
