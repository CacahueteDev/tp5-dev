# TP5

## II. Opti Calculatrice

### 1. Strings sur mesure

> Fichier: [tp5_enc_server_1.py](tp5_enc_server_1.py)

Client
```shell
Tapez un calcul :3+1
Réponse reçue du serveur 127.0.0.1 : 4
```

### 2. Code Encode Decode

> Fichier: [tp5_enc_client_2.py](tp5_enc_client_2.py)

Client
```shell
Tapez un calcul :3+1
Réponse reçue du serveur 127.0.0.1 : 4
```

## III. HTTP

### 1. Serveur Web

> Fichier: [tp5_web_serv_1](tp5_web_serv_1.py)

```shell
$ curl 127.0.0.1:80
<h1>Hello je suis un serveur HTTP</h1>
```

Et ça marche niquel avec mon navigateur

### 2. Client Web

> Fichier: [tp5_web_client_2](tp5_web_client_2.py)

Serveur
```
started server on :80
client connected: ('127.0.0.1', 55445)
```

Client
```
Réponse du serveur HTTP: HTTP/1.0 200 OK

<h1>Hello je suis un serveur HTTP</h1>

```

### 3. Délivrer des pages web

> Fichier: [tp5_web_serv_3](tp5_web_serv_3.py)

### 4. Quelques logs

> Fichier: [tp5_web_serv_4](tp5_web_serv_4.py)